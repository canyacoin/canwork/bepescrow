# BEPScrow - Hedged escrow service for BEP2 tokens on Binance Chain

BEPScrow is a hedged escrow service for BEP2 tokens on Binance Chain. Currently for experimental purposes only.

### Project stack:

- Nodejs
- GitLab CI
- Heroku

### Prerequisites

```
yarn
node v8^
```

### Env variables

Create `.env` file and set the following variables.

```
CHAIN_ADDRESS=
CHAIN_PRIVKEY=
PORT=
CHAIN_WSURI=wss://testnet-dex.binance.org/api/ws/
CHAIN_APIURI=https://testnet-dex.binance.org/
CHAIN_NET=  // Binanace chain eg. testnet or mainnet
BEP2_SYMBOL= // eg. TCAN-014
GECKO_COINID= // (coingecko coinid) eg. canyacoin
CHAIN_START=    // Where to scan binance /transfers api from in ms. eg. 1546300800000
```

### Project Setup

```
yarn install
yarn start
```

## CI/CD

GitLab CI

- Deploy

Main Branch:

- master
