/**
 * Local Imports
 */
require('dotenv').config()

const app = require('./config/config').app
const utils = require('./app/utils');
const binance = require('./app/binance');
const escrow = require('./app/escrow');
const websocket = require('./app/websocket');
const markets = require('./app/markets');
const sync = require('./app/sync');
const api = require('./app/api')

/**
 * Runs all the functions necessary to start the bot.
 */
console.log(Date.now())
const main = async() => {
    utils.winston.info("STARTING")

    app.state = false;
    app.symbolPrice = 0;
    app.jobs = [];
    app.backlog = [];
    app.admins = [];
    app.binanceRateLimSecond = 5;
    app.binanceRateLimMin = 60;

    await markets.getSymbolPrice();
    await binance.binanceConnect();
    await binance.getSequence();
    sync.syncState();
    escrow.pollBacklog();
}

  
// Get symbol price every minute
setInterval(x => {
    markets.getSymbolPrice();
},60000)


main();
