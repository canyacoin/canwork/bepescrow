/* Imports */
const axios = require('axios');
const app = require('../config/config').app


/**
 * Gets market data for tokens
 * @param   {String}     CoinId from coingecko
 * @return  {Number}     USD market price.
 */
const loadGeckoData = async(coinId) => {
    try {
        const url = `https://api.coingecko.com/api/v3/coins/${coinId}`;
        const coingecko = await axios.get(url)
        const marketPrice = coingecko.data.market_data.current_price.usd.toFixed(4)
        return marketPrice;
    } catch(err){
        console.log(err)
        utils.logger('ERROR_GECKO')
        app.state = false;
        return error;
    }    
}

/* Gets price data for symbol to calculate hedged escrow */
const getSymbolPrice = async() => {
    app.symbolPrice = await loadGeckoData(app.GECKO_COINID)
}


/* EXPORTS */
exports.getSymbolPrice = getSymbolPrice