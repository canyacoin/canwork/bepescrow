const express = require('express')
const utils = require('./utils')
const api = express();
const app = require('../config/config').app

api.get('/', (req,res) =>{
    let endpoints = {
        "all jobs": '/jobs',
        "active": '/jobs/active',
        "disbursed": '/jobs/disbursed',
        "refunded": '/jobs/refunded',
        "Single Job": '/job/{jobNo}'
        }
    res.send(endpoints)
})

api.get('/jobs', async(req,res) =>{
    let stats = utils.getSyncStats()
    let jobs = {
        status: (utils.syncing() ? 'SYNCING' : 'LIVE'),
        stats:{
            total: app.jobs.length,
            active: stats.escrow,
            disbursed:  stats.closed,
            refunded:   stats.refunded
        },
        jobs:app.jobs
    }
    res.send(jobs);
})

api.get('/jobs/active', async(req,res) =>{
    let stats = utils.getSyncStats()
    let jobs = {
        status: (utils.syncing() ? 'SYNCING' : 'LIVE'),
        stats:{
            total: app.jobs.length,
            active: stats.escrow,
            disbursed:  stats.closed,
            refunded:   stats.refunded
        },
        active: app.jobs.filter(x => x.status == 'ACTIVE')
    }
    res.send(jobs);
})

api.get('/jobs/disbursed', async(req,res) =>{
    let stats = utils.getSyncStats()
    let jobs = {
        status: (utils.syncing() ? 'SYNCING' : 'LIVE'),
        stats:{
            total: app.jobs.length,
            active: stats.escrow,
            disbursed:  stats.closed,
            refunded:   stats.refunded
        },
        disbursed: app.jobs.filter(x => x.status == 'DISBURSED')
    }
    res.send(jobs);
})

api.get('/jobs/refunded', async(req,res) =>{
    let stats = utils.getSyncStats()
    let jobs = {
        status: (utils.syncing() ? 'SYNCING' : 'LIVE'),
        stats:{
            total: app.jobs.length,
            active: stats.escrow,
            disbursed:  stats.closed,
            refunded:   stats.refunded
        },
        refunded: app.jobs.filter(x => x.status == 'REFUNDED')
    }
    res.send(jobs);
})

api.get('/job/:jobNo', async(req,res) =>{
    let job = app.jobs.filter(x => x.id == req.params.jobNo);
    if(job){
        res.send(job[0]);
    }
})

api.listen(app.EXPRESS_PORT)