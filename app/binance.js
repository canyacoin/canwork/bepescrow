/* Imports */
const BnbApiClient = require('@binance-chain/javascript-sdk');
const axios = require('axios');
const app = require('../config/config').app;
const utils = require('./utils');

var bnbClient = {};

/* Start Binance Chain Client */
const binanceConnect = async() => {
    try{
        bnbClient = new BnbApiClient(app.CHAIN_APIURI)
        bnbClient.chooseNetwork(app.CHAIN_NET);
        await bnbClient.initChain()
        await bnbClient.setPrivateKey(app.CHAIN_PRIVKEY)
        utils.winston.info("### WALLET CONNECTED: ")
        return bnbClient;
    } catch(err){
        console.log(err)
        utils.winston.info("ERROR SETTING PRIVATE KEY")
        return err;
    }
}


/* Transfer tokens on binnace chain */
const transferToken = async(job, txType, force) =>{
    if(utils.syncing() && !force){ return true}
        switch (txType) {
            case 'refund':      M = `REFUND:${(job.id || job.txHash)}`; break;
            case 'disburse':    M = `DISBURSE:${job.id}:${job.payout.value}`; break;
            case 'price':       M = `VALUE:${job.id}:${job.escrow.value}`; break;
            default: break;
        }
        o = job.txFrom || job.payout.payee;
        a = job.txTkn || job.payout.asset;
        A = job.txQty || job.payout.amount;
    try{
        let thisSeq = await getSequence()
        const res = await bnbClient.transfer(app.CHAIN_ADDRESS, o, A, a, M, thisSeq)
        const hash = res.result[0].hash;
        return hash;
    } catch(err){
        console.log(err)
        utils.logger('RELEASE_FAILED', job.txHash);
        let error = utils.makeError('RELEASE_FAILED')
        return error;
    }
}


/* Get transactions for a binance chain address */
async function getTransactions(epochStart, epochEnd, offset){
    let url = app.CHAIN_APIURI+
        'api/v1/transactions?startTime='+epochStart+
        '&endTime='+epochEnd+
        '&txType=TRANSFER&address='+app.CHAIN_ADDRESS+
        '&offset='+offset;
    try {
        await utils.rateLimitCheck()
        let res = await axios.get(url)
        utils.updateRateLimits(res.headers['x-ratelimit-remaining-minute'],res.headers['x-ratelimit-remaining-second'])
        return res;
    } catch(err){
        return err;
    }
}

/* Get weighted average price for an asset */
const getPrice = async(symbol) => {
    let url = app.CHAIN_APIURI+
        'api/v1/ticker/24hr?symbol='+symbol
    try {
        await utils.rateLimitCheck()
        let res = await axios.get(url)
        utils.updateRateLimits(res.headers['x-ratelimit-remaining-minute'],res.headers['x-ratelimit-remaining-second'])
        return res.data.weightedAvgPrice;
    } catch(err){
        return err;
    }
}

/* Get sequence of account */
const getSequence = async() =>{
    try{
        let url = app.CHAIN_APIURI + 'api/v1/account/'+app.CHAIN_ADDRESS+'/sequence';
        let res = await axios.get(url)
        return res.data.sequence;
    } catch(err){
        app.state = false;
        return err
    }
}

/* Check Address */
const checkAddress = (address) =>{
    return bnbClient.checkAddress(address)
}

/* EXPORTS */
exports.getSequence = getSequence;
exports.transferToken = transferToken;
exports.binanceConnect = binanceConnect;
exports.getTransactions = getTransactions;
exports.checkAddress = checkAddress;
exports.getPrice = getPrice;