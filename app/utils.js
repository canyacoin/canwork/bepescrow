/* Local Imports */
const app = require('../config/config').app;
const winston = require('winston');
const binance = require('./binance')

winston.add(new winston.transports.Console())

const logStrings = {
  PARSE_ERROR:      'ESCROW: INCOMING TX IGNORED BECAUSE THE OBJECT COULD NOT BE PARSED. HASH: {1}',
  MEMO_EMPTY:       'ESCROW: INCOMING TX IGNORED DUE TO EMPTY MEMO. HASH: {1}',
  MEMO_BAD:         'ESCROW: INCOMING TX IGNORED DUE TO BAD MEMO INSTRUCTION. HASH: {1}',
  PROVIDER_BAD:     'ECSROW: INCOMING TX IGNORED DUE TO BAD PROVIDER ADDRESS. HASH {1}',
  ESCROW_DUPLICATE: 'ESCROW: CREATE ESCROW ABORTED. REASON: DUPLICATE JOB. HASH: {1}',
  ESCROW_SUCCESS:   'ESCROW: SUCCESSFULLY CREATED ESCROW FOR JOB: {1}',
  RELEASE_NOJOB:    'ESCROW: RELEASE ABORTED. REASON: JOB NOT FOUND. HASH: {1}',
  RELEASE_DUPLICATE:'ESCROW: RELEASE ABORTED. REASON: ALREADY RELEASED. HASH: {1}',
  RELEASE_NOTAUTH:  'ESCROW: RELEASE ABORTED. REASON: NOT AUTHORISED. JOB: {1}',
  RELEASE_NOPRICE:  'ESCROW: RELEASE ABORTED. REASON: CANT GET PRICE DATA. JOB: {1}',
  RELEASE_SUCCESS:  'ESCROW: RELEASE SUCCESSFUL FOR JOB: {1}',
  RELEASE_FAILED:   'ESCROW: RELEASE FAILED FOR JOB: {1}',
  REFUND_FAILED:    'ESCROW: REFUND FAILED FOR HASH: {1}',
  REFUND_SUCCESS:   'ESCROW: REFUND SUCCESS FOR HASH: {1}',
  ERROR_TRANSFER:   'ERROR IN BINANCE TRANSFER',
  PENDING_TX:       'ESCROW: INCOMING TX QUEUED DUE TO STATE OFF. HASH: {1}',
  ERROR_SEQ:        'ERROR GETTING SEQUENCE NUMBER',
  ERROR_GECKO:      'ERROR GETTING MARKET DATA FROM COINGECKO',
  ERROR_TX:         'ERROR GETTING TRANSACTION DATA FROM BINANCE API',
  BACKLOG_START:    'SYNCING: CLEARING BACKLOG...',
  BACKLOG_END:      'SYNCING: BACKLOCK CLEARED! ONLINE.',
  FUND_LATE:        'FUND: IGNORED FUNDING DUE TO ESCROW STARTED. HASH: {1}',
  FUND_SUCCESS:     'FUND: ACCEPTED FUNDING. HASH: {1}',
  ADMIN_SUCCESS:    'FUND: ADMIN ADDED. ADDRESS: {1}',
  REFUND_NOTAUTH:   'REFUND: NOT AUTH. HASH: {1}',
  REFUND_AUTH:      'REFUND: REFUND REQUESTED. HASH {1}',
  REFUND_BADJOB:    'REFUND: REFUND IGNORED. JOB NOT FOUND. HASH {1}',
  REFUND_BADSTATUS: 'REFUND: REFUND IGNORED. JOB NOT ACTIVE. HASH {1}',
  REFUND_ERROR:     'REFUND: REFUND IGNORED. NOT AUTHED, BAD STATUS OR NO JOB. HASH {1}',
  PRICE_SUCCESS:    'PRICE: ESCROW SUCCESSFULLY PRICED ${1}',
  PRICE_NOJOB:      'PRICE: NO JOB FOUND ${1}',
  PRICE_NOTACTIVE:  'PRICE: JOB NOT ACTIVE ${1}',
  PRICE_NOTAUTH:    'PRICE: NOT AUTHORISED ${1}',
  PRICE_BAD:        'PRICE: BAD PRICE. ${1}',
  PRICE_FAIL:       'PRICE: TX FAILED. ${1}'
  
}


/* Logging helper */
const logger = (err, data) => {
  var string = logStrings[err] || logStrings[err.message] || 'UNHANDLED ERROR!\n'+err;
  return syncing()
  ? winston.info('[REPLAY] '+ string.replace('{1}',data))
  : err.message !== 'STREAM_BAD'
    ? winston.info(string.replace('{1}',data))
    : false;
}


/* Error helper */
const makeError = (message) => {
  throw new Error(message);
}


/* Push to escrow array helper */
const updateEscrow = (escrowObj) => {
  app.escrow = app.escrow.filter(x => x.jobNo !== escrowObj.jobNo)
  app.escrow.push(escrowObj)
}


/* Update rate limits helper */
const updateRateLimits = (min,sec) => {
  app.binanceRateLimSecond = sec || 0 ;
  app.binanceRateLimMin = min || 0;
  let rateLimStats = 'RATE LIMIT REMAINING: [MIN]:'+app.binanceRateLimMin+' [SEC]:'+app.binanceRateLimSecond;
}

const rateLimitCheck = async() =>{
  await new Promise(resolve => {
    let sec = app.binanceRateLimSecond;
    let min = app.binanceRateLimMin;
    let ms = min < 10 || sec <2 ? 1000 : 100;
    setTimeout(resolve, ms)
  });
}

/* stall helper  */
const stall = async(ms=500) => {
  await new Promise(resolve => setTimeout(resolve, ms));
}


 /**
 * Splits binanace chain memo by colon separator
 * @param   {Object} msg    Telegram Message Object
 * @return  {Object}        Object containing text, command & arguments
 */
  function splitMsg(msg){
      try{
      const text = msg;
      const match = text.match(/^([^:]+):?(.+)?/)
      let args = []
      let command
      if (match !== null) {
        if (match[1]) {
          command = match[1].toUpperCase()
        }
        if (match[2]) {
          args = match[2].split(':')
        }
      }

      let msgObj = {
        raw: text,
        command,
        args
      }

      return msgObj || false;
    } catch(e){
      return false;
    }
  }


/**
* Takes a binance chain payload (or API) & returns a neat object
* @param   {Object}   payload    Binance payload object
* @return  {Object}              Neat object
*/
function parseTransaction(payload){
  let tx = {}
  try{
    tx.txHash   = payload.txHash            || payload.data.H
    tx.txTo     = payload.toAddr            || payload.data.t[0].o
    tx.txFrom   = payload.fromAddr          || payload.data.f
    tx.txTkn    = payload.txAsset           || payload.data.t[0].c[0].a
    tx.txQty    = Number(payload.value)     || Number(payload.data.t[0].c[0].A)
    tx.txMemo   = payload.txHash 
      ? payload.memo
        ? payload.memo
        : null
      : (payload.data.M ? payload.data.M : null)

    tx.txMemo = splitMsg(tx.txMemo)
    return tx
  } catch (e){
    console.log(e)
    return null;
  }
}


/* Build dates helper */
const getPeriods = () => {
  var epochs = {}
  epochs.now = Date.now();
  epochs.genesis = Number(app.CHAIN_START);
  epochs.msIn28Days = 2419200000;
  app.syncTill = epochs.now;

  // Make an array of period objects
  var periods = [];
  const periodsOf28Days = Number(((epochs.now-epochs.genesis)/epochs.msIn28Days).toFixed(0))+1
  for (let i = 0; i < periodsOf28Days; ++i) {
    periods.push({id: i+1,start: (epochs.now-(epochs.msIn28Days*(i+1))+1), end: (epochs.now-(epochs.msIn28Days*i))})
  }

  return periods;
}


/* Sync stats helper */
const getSyncStats = () => {
  var stats = {}
  stats.escrow = app.jobs.filter(x => x.status == 'ACTIVE').length
  stats.release = app.jobs.filter(x => x.status == 'RELEASING').length
  stats.closed = app.jobs.filter(x => x.status == 'DISBURSED').length
  stats.refunded = app.jobs.filter(x => x.status == 'REFUNDED').length
  return stats;
}

/* Get # of jobs/escrows */
const countJobs = () => {
  return app.jobs.length;
}

/**
* Takes a binance chain payload (or API) & checks it can be handled.
* @param   {Object}   payload    Binance payload object
* @return  {Promise}             Promise
*/
const validatePayload = async(payload) => {
    let txIsTransferStream = h.txIsTransferStream(payload)  || makeError('STREAM_BAD')
    let tx = parseTransaction(payload)                      || makeError('PARSE_ERROR')
    let txIsValid = h.txIsValid(tx)                         || makeError('PARSE_ERROR')
    let txHasMemo = h.txHasMemo(tx)                         || makeError('MEMO_EMPTY')
    let txHasValidMemo = h.txHasValidMemo(tx)               || makeError('MEMO_BAD')

    return validPayload = await Promise.all([
        txIsTransferStream,
        txIsValid,
        txHasMemo,
        txHasValidMemo
        ])
} 


/**
* Takes a parsed escrow tx and validates against defined logic
* @param   {Object}   tx    Parsed transaction object
* @return  {Promise}        Promise
*/
const validateEscrow = async(tx) => {
    let txMemoHasArgs = h.txMemoHasArgs(tx)         || makeError('MEMO_BAD')
    let payeeIsValid = h.payeeIsValid(tx)           || makeError('PROVIDER_BAD')
    let escrowIsNew = h.escrowIsNew(tx)             || makeError('ESCROW_DUPLICATE')
    
    return validEscrow = await Promise.all([
      txMemoHasArgs,
      payeeIsValid,
      escrowIsNew
      ])
} 


/**
* Takes a parsed release tx and validates against defined logic
* @param   {Object}   tx    Parsed transaction object
* @return  {Promise}        Promise
*/
const validateRelease = async(tx) => {
  let escrowIsValid = h.escrowIsValid(tx)           || makeError('RELEASE_NOJOB')
  let escrowIsLive = h.escrowIsLive(tx)             || makeError('RELEASE_DUPLICATE')
  let escrowIsAuth = h.escrowIsAuth(tx)             || makeError('RELEASE_NOAUTH')

  return validEscrow = await Promise.all([
    escrowIsValid,
    escrowIsLive,
    escrowIsAuth
      ])
} 

/* Validate Price */
const validatePrice = async(tx) => {
  let escrowIsValid = h.escrowIsValid(tx)                     || makeError('PRICE_NOJOB')
  let escrowIsLive = h.escrowIsLive(tx)                       || makeError('PRICE_NOTACTIVE')
  let txFromEscrowService = h.txFromEscrowService(tx)         || makeError('PRICE_NOTAUTH')
  let isValidPrice = h.isValidPrice(tx.txMemo.args[1])        || makeError('PRICE_BAD')

  return validEscrow = await Promise.all([
    escrowIsValid,
    escrowIsLive,
    txFromEscrowService,
    isValidPrice
      ])
} 

/* Find job helper */
const findJob = (jobNo) => {
  let job = app.jobs.filter(x => x.id == jobNo)
  return job[0] ? job[0] : false;
}

/* Find job index helper */
const findIndex = (jobid) => {
  let index = app.jobs.findIndex(x => x.id == jobid)
  return index < 0 ? false : index;
}


/* Get the current price helper */
const nowPrice = () => {
  return Number(app.symbolPrice);
}

/* Check if price */
const isValidPrice = (price) => {
  return isNaN(price.split('$')[1]) ? false : true;
}

/* Calcuate how many tokens to release from escrow helper */
const calcTkns = (usdAmt) => {
  return Number((usdAmt/ nowPrice()).toFixed(4))
}
/* Validation helpers */
const h = {
  txIsTransferStream: (payload) => {
    return payload.stream
  ? payload.stream == 'transfers'
    ? true
    : false
  : payload.txHash // because the binance API has no stream. 
    ? true
    : false
  },

  txIsIncoming: (payload) => {
    payload.data
  ? payload.data.t[0].o == app.CHAIN_ADDRESS
    ? true
    : false
  : payload.fromAddr !== app.CHAIN_ADDRESS
    ? true
    : false
  },

  txHasValidMemo:     (tx) => {
    let verbs = ['ESCROW','RELEASE','DISBURSE','REFUND','FUND','REFUND','CANCEL','PRICE','ADD']
    return verbs.includes(tx.txMemo.command) ? true : false
  },

  txIsRefund:     (tx) => {
    return tx.txMemo.command == 'REFUND' ? true : false
  },

  escrowIsAuth:       (tx) => {
    let escrow = findJob(tx.txMemo.args[0])
    return (escrow.status == 'ACTIVE' && escrow.escrow.payer == tx.txFrom) ? true : false
  },

  txIsValid:          (tx) => { return tx && tx.txHash ? true : false },
  txHasMemo:          (tx) => { return tx && tx.txMemo ?  true :  false },
  txMemoHasArgs:      (tx) => { return tx.txMemo.args && tx.txMemo.args.length == 3 ? true : false },
  payeeIsValid:       (tx) => { return binance.checkAddress(tx.txMemo.args[2])},
  escrowIsNew:        (tx) => { return findJob(tx.txMemo.args[0]) ? false : true },
  escrowIsValid:      (tx) => { return findJob(tx.txMemo.args[0]) ? true : false },
  escrowIsLive:       (tx) => { return findJob(tx.txMemo.args[0]).status == 'ACTIVE' ? true : false },
  serviceIsLive:      () => { return app.state ? true : false }
}

const syncing = () =>{
  return app.state ? false : true 
}

const txFromEscrowService = (tx) => {
  return tx.txFrom == app.CHAIN_ADDRESS ? true : false;
}

const isAdmin = (address) => {
  return app.admins.filter(x => x.address == address)[0] ? true : false;
}


/* EXPORTS */
exports.winston = winston;
exports.splitMsg = splitMsg;
exports.logger = logger;
exports.parseTransaction = parseTransaction;
exports.h = h;
exports.validatePayload = validatePayload;
exports.validateEscrow = validateEscrow;
exports.validateRelease = validateRelease;
exports.updateEscrow = updateEscrow;
exports.getPeriods = getPeriods;
exports.getSyncStats = getSyncStats;
exports.findJob = findJob;
exports.isAdmin = isAdmin;
exports.findIndex = findIndex;
exports.updateRateLimits = updateRateLimits;
exports.rateLimitCheck = rateLimitCheck;
exports.makeError = makeError;
exports.stall = stall
exports.syncing = syncing
exports.nowPrice = nowPrice;
exports.calcTkns = calcTkns;
exports.countJobs = countJobs;
exports.txFromEscrowService = txFromEscrowService
exports.validatePrice = validatePrice