/* External Imports */
const WebSocket = require('ws');

/* Local Imports */
const app = require('../config/config').app;
const utils = require('./utils');
const escrow = require('./escrow');


/**
 * Binance Chain Websocket events
 */
const connectWebsocket = () => {
    var ws = new WebSocket(app.CHAIN_WSURI+app.CHAIN_ADDRESS);
    ws.on('message',    incoming = (data)   => { 

        // We aren't using block height to switch from sync > live.
        // We sync up to a given epoch but are listening for tx from that epoch. Works fine for now.
        if(Date.now() >= app.syncTill){
            app.backlog.push(JSON.parse(data))
        }
    });
    ws.on('open',       open = ()           => { utils.winston.info('WS: CONNECTED')});
    ws.on('error',      error = (e)         => { utils.winston.warn('WS: ERRORED: '+e)});
    ws.on('close',      close = (e)         => { utils.winston.warn('WS: CLOSED WITH ERROR: '+e);wsReconnect();});
    ws.on('ping',       ping = (data)       => { utils.winston.silly('WS: PING RECEIVED'); ws.isAlive = true;});
}


/* Handle ws drop/reconnect */
function wsReconnect(wsRecon){
        setTimeout(x => {
            return connectWebsocket()
        },1000)
}


/* MAIN */
connectWebsocket()


/* EXPORTS */
exports.connectWebsocket = connectWebsocket;