/* Local Imports */
const app = require('../config/config').app;
const utils = require('./utils');
const binance = require('./binance');
const h = utils.h;

/**
 * Processes the payload received from the Binance websocket
 * @param  {Object} payload     The websocket message payload
 * Refer https://docs.binance.org/api-reference/dex-api/ws-streams.html#3-transfer
 */
const handlePayload = async(payload) => {
    var commands = {
        "ESCROW":       newEscrow,
        "RELEASE":      releaseEscrow,
        "DISBURSE":     closeEscrow,
        "FUND":         fundService,
        "REFUND":       refundEscrow,
        "PRICE":        priceEscrow,
        "ADD":          addEscrow
    }

    try{
        await utils.validatePayload(payload)
        let tx = utils.parseTransaction(payload)
        return await commands[tx.txMemo.command](tx);
    } catch(error){
        utils.logger(error);
        return error
    }
}


/* Increase the value of an escrow */
const addEscrow = async(tx) => {
return true;
}

/* Confirm the price of an escrow */
const priceEscrow = async(tx) => {
    try{
        await utils.validatePrice(tx)
        let jobIndex = utils.findIndex(tx.txMemo.args[0])
        app.jobs[jobIndex].updatePrice(tx)
        utils.logger('PRICE_SUCCESS',tx.txMemo.args[0]);
        return app.jobs[jobIndex];
    }
    catch(error){
        utils.logger(error, tx.txHash);
        return error;
    }
}


/* New escrow */
const newEscrow = async(tx) => {
    try {
        await utils.validateEscrow(tx)
        app.jobs.push(new Escrow(tx))
        utils.logger('ESCROW_SUCCESS',tx.txMemo.args[0]); 
        // tx.txPrice = await binance.getPrice(`${tx.txTkn}_BNB`)
        // await binance.transferToken(tx,'price')
        //     .then(refund => utils.logger('PRICE_SUCCESS',tx.txHash))
        //     .catch(error => utils.logger('PRICE_FAILED',tx.txHash))
        return app.jobs[utils.findIndex(tx.txMemo.args[0])]
    } catch (error) {
        utils.logger(error, tx.txHash);
        await binance.transferToken(tx,'refund')
            .then(refund => utils.logger('REFUND_SUCCESS',tx.txHash))
            .catch(error => utils.logger('REFUND_FAILED',tx.txHash))
        return error;
    }   
}

/* Refund request from admin/payee */
const refundEscrow = async(tx) => {
    try{
        if(utils.txFromEscrowService(tx)){ 
            let job = utils.findJob(tx.txMemo.args[0])     
            if(job && job.status == 'REFUNDING'){
                let jobIndex = utils.findIndex(tx.txMemo.args[0])
                app.jobs[jobIndex].finaliseRefund(tx)
                return app.jobs[jobIndex]
            } else { return true}
        } else {      
            let job = utils.findJob(tx.txMemo.args[0])
            if((utils.isAdmin(tx.txFrom) || job.payout.payee == tx.txFrom) && job && job.status == 'ACTIVE'){
                try{
                    let jobIndex = utils.findIndex(tx.txMemo.args[0])
                    app.jobs[jobIndex].refund(tx)
                    let txHash = await binance.transferToken(app.jobs[jobIndex],'refund')
                    utils.logger('REFUND_SUCCESS',txHash);
                    return app.jobs[jobIndex];
                }
                catch(error){
                    utils.logger(error, tx.txHash);
                    return error;
                }
            } else {
                return utils.makeError('REFUND_ERROR')
            }
        }
    } catch(error){
        //console.log(error)
        return error;
    }
}

/* Fund the escrow service & add admins */
const fundService = async(tx) => {
    if(utils.countJobs()){
        return utils.makeError('FUND_LATE')
    } else {
        app.admins.push({address:tx.txFrom, hash:tx.txHash})
        utils.logger('FUND_SUCCESS',tx.txHash)
        utils.logger('ADMIN_SUCCESS',tx.txFrom)
        for await (args of tx.txMemo.args){
            let thisAdmin = await binance.checkAddress(args);
            if(thisAdmin){
                app.admins.push({address:args, hash:tx.txHash})
                utils.logger('ADMIN_SUCCESS',args)
            }
        }
        return app.admins;
    }
}


/**
 * Releases an Escrow
 * @param  {Object}     tx          The beautified binanace transaction object.
 */
const releaseEscrow = async(tx) => {
    try{
        await utils.validateRelease(tx)
        let jobIndex = utils.findIndex(tx.txMemo.args[0])
        app.jobs[jobIndex].release(tx)
        let txHash = await binance.transferToken(app.jobs[jobIndex],'disburse',null)
        utils.logger('RELEASE_SUCCESS',tx.txMemo.args[0]);
        return app.jobs[jobIndex];
    }
    catch(error){
        utils.logger(error, tx.txHash);
        return error;
    }
}


/* Close out an escrow after being released */
const closeEscrow = async(tx) =>{
    try{
        let job = utils.findJob(tx.txMemo.args[0]);
        let jobIndex = utils.findIndex(tx.txMemo.args[0])

        if(!h.txIsIncoming(tx) && job && job.status == 'RELEASING'){
            app.jobs[jobIndex].close(tx); 
            return app.jobs[jobIndex];
        }
        return tx;
    } catch(e){
        console.log(e)
    }
}

/* Escrow Class */
var Escrow = function(tx) {
    let {txFrom, txMemo, txHash, txQty, txTkn } = tx;
    let time = Date.now();

    /* Top Level */
    this.id =     txMemo.args[0],
    this.status = 'ACTIVE',

    /* Escrow */
    this.escrow = {
        amount: txQty,
        asset:  txTkn,
        value:  0.05,
        payer:  txFrom,
        time:   Date.now(),
        hash:   txHash,
    },

    /* Payout */
    this.payout = {
        amount: null,
        asset:  txTkn,
        value:  0.05,
        payee:  tx.txMemo.args[2],
        time:   null,
        hash:   null,
    },

    /* Events */
    this.events = [{
        event:  'ESCROW',
        hash:   txHash,
        time:   time
    }]
}


/* Release */
Escrow.prototype.release = function(tx){
    let {txFrom, txMemo, txHash, txQty, txTkn } = tx;
    let time = Date.now();

    this.status = 'RELEASING'
    this.payout.amount = utils.calcTkns(this.escrow.value),
    this.payout.time = null;
    this.payout.hash = null;
    this.events.push({
        event: 'RELEASE',
        hash: txHash,
        time: time
    })
}

/* Confirm Released */
Escrow.prototype.close = function(tx){
    let time = Date.now();
    let {txHash} = tx

    this.status = 'DISBURSED'
    this.payout.time = time;
    this.payout.hash = txHash;
    this.events.push({
        event: 'DISBURSE',
        hash: txHash,
        time: time
    })

}

/* Refund */
Escrow.prototype.refund = function(tx){
    let {txFrom, txMemo, txHash, txQty, txTkn } = tx;
    let time = Date.now();

    this.status = 'REFUNDING'
    this.payout.amount = utils.calcTkns(this.escrow.value),
    this.payout.time = time;
    this.payout.hash = null;
    this.events.push({
        event: 'REFUND REQUEST',
        hash: txHash,
        time: time
    })
}

/* Confirm Refund */
Escrow.prototype.finaliseRefund = function(tx){
    let {txFrom, txMemo, txHash, txQty, txTkn } = tx;
    let time = Date.now();

    this.status = 'REFUNDED'
    this.payout.hash = txHash;
    this.events.push({
        event: 'REFUND',
        hash: txHash,
        time: time
    })
}

/* Update Price */
Escrow.prototype.updatePrice = function(tx){
    let {txFrom, txMemo, txHash, txQty, txTkn } = tx;
    let time = Date.now();
    let value = txMemo.args[1];
    this.events.push({
        event: 'PRICE',
        hash: txHash,
        time: time
    })
}



/* Poll backlog for transactions to process */
const pollBacklog = async() =>{
    setInterval(async()=> {
        if(h.serviceIsLive()){
            let thisPayload = app.backlog.shift();
            if(thisPayload){
                return await handlePayload(thisPayload, false)
            }
        }
    }, 2000);
}

/* EXPORTS */
exports.handlePayload = handlePayload;
exports.pollBacklog = pollBacklog;