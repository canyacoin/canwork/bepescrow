/* Local Imports */
const app = require('../config/config').app;
const utils = require('./utils');
const binance = require('./binance');
const escrow = require('./escrow');
const fs = require('fs');

/**
 * Syncs transactions on the blockchain
 */
var syncState = async() => {
    var transactions = [];
    var periods = utils.getPeriods();

    utils.winston.info('SYNCING: ['+periods.length+'] TRANSACTION PERIODS.');

    // Get transactions for each period with offset 0 (first page)
    for await (period of periods){
        // Find out how many offsets within this period
        let data = await binance.getTransactions(period.start, period.end, 0)
        period.total = data.data.total;
        period.totalOffsets = Number((data.data.total/25).toFixed(0)) // binance tx per page.
        period.offsets = [];
        // create array of offset objects inside this period
        for (let i = 0; i < period.totalOffsets; ++i) {
            period.offsets.push({id: i+1,start: period.start, end: period.end, offset: i*25})
        }
        utils.winston.info(`SYNCING: ${period.id}/${periods.length} [${period.total}] TRANSACTIONS FOUND.`);
    }

    periods.txTotal = periods.reduce((acc,period) => { return period.total+acc},0)
    utils.winston.info(`SYNCING: [${periods.reduce((acc,period) => { return period.total+acc},0)}] TRANSACTIONS FOUND.`);

    // Get transactions for remaining pages now we have total tx count & offsets
    for await (period of periods){
            for await (offset of period.offsets){
                let data = await binance.getTransactions(period.start, period.end, offset.offset)
                for (tx of data.data.tx){ transactions.push(tx)}
            }

            // temp workaround for single pages. TODO: refactor.
            if(period.offsets.length <1){
                let data = await binance.getTransactions(period.start, period.end, 0)
                for (tx of data.data.tx){ transactions.push(tx)}
            }
    }
    
    // Finally reverse the order so we can process them in the right order (oldest to newest)
    transactions = transactions.reverse()
    utils.winston.info('SYNCING: ['+transactions.length+'] TRANSACTIONS FOUND OVER '+(periods.length)+' MONTHS. PROCESSING.');

    // Replay all the transactions to get current status of escrow jobs & release any missed transactions.
    try{
        for await (tx of transactions) {await escrow.handlePayload(tx,true);}    
        await releaseOrphans();

        var stats = utils.getSyncStats()
        utils.winston.info('SYNCING: ['+app.jobs.length+'] JOBS PROCESSED SUCCESSFULLY.');
        utils.winston.info('[ACTIVE]:'+stats.escrow+' [RELEASED]:'+stats.release+' [CLOSED]:'+stats.closed+' [REFUNDED]: '+stats.refunded)
        return app.state = true;
    } catch(error){
        console.log(error)
        return utils.winston.info('SYNCING: FAILED! SERVICE OFFLINE.');
    }
}


/**
* Releases any orphaned jobs. Ie. Escrows we were supposed to release but failed to.
* @return  {Boolean}             True/False
*/
const releaseOrphans = async() => {
    var orphanJobs = app.jobs.filter(x => x.status == 'RELEASING')
    utils.winston.info('SYNCING: ['+orphanJobs.length+'] ORPHANED JOBS FOR RELEASE.');
    for await (job of orphanJobs) {
        try{
            job.payout.hash = await binance.transferToken(job,'disburse',true)
            job.status = 'DISBURSED';
            job.amount = 1;
            utils.logger('RELEASE_SUCCESS',job.id);
        } 
        catch (error){

            // One failed tx would result in the entire service being offline.
            // TODO: reconsider
            console.log(error)
            utils.logger('RELEASE_FAILED',job.jobNo);
            return false;
        }
        await utils.stall(1000)
    }
    return true;
}


/* EXPORTS */
exports.syncState = syncState;