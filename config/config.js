const config = {
    CHAIN_WSURI:    process.env.CHAIN_WSURI,
    CHAIN_APIURI:   process.env.CHAIN_APIURI,
    CHAIN_START:    process.env.CHAIN_START_TEST,
    CHAIN_ADDRESS:  process.env.CHAIN_ADDRESS,
    CHAIN_PRIVKEY:  process.env.CHAIN_PRIVKEY,
    TEST_ADDRESS:   process.env.TEST_ADDRESS,
    TEST_PRIVKEY:   process.env.TEST_PRIVKEY,
    TEST2_ADDRESS:  process.env.TEST2_ADDRESS,
    TEST2_PRIVKEY:  process.env.TEST2_PRIVKEY,
    CHAIN_NET:      process.env.CHAIN_NET,
    EXPRESS_PORT:   process.env.PORT,
    BEP2_SYMBOL:    process.env.BEP2_SYMBOL,
    GECKO_COINID:   process.env.GECKO_COINID
}

exports.app = config;