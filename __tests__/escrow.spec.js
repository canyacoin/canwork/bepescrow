const app = require('../config/config').app
const binance = require('../app/binance')
const escrow = require('../app/escrow')
const fs = require('fs')

app.jobs = []
app.admins = []
app.state = true;
app.symbolPrice = 0.02;
app.CHAIN_ADDRESS = 'tbnb1uu44f9sdv3ka957624zzxcfcvvwmjgc6tncchf'

const testdata = JSON.parse(fs.readFileSync('./__mockData__/escrow.json'))

/* Setup jobs - Active Escrow*/
const newEscrow = async() => {
  await binance.binanceConnect()
  let payload = testdata.inputs.escrow;
  await escrow.handlePayload(payload, null)
}

/* Setup jobs - Released Escrow*/
const releasedEscrow = async() => {
  await binance.binanceConnect()
  await newEscrow();

  payload = testdata.inputs.release;
  await escrow.handlePayload(payload, null)
}

/* Setup jobs - Closed Escrow*/
const closedEscrow = async() => {
  await binance.binanceConnect()
  await releasedEscrow();

  payload = testdata.inputs.disburse;
  await escrow.handlePayload(payload, null)
}



describe('SMOKE TESTS', () => {
  beforeAll(async() => {
    return await binance.binanceConnect()
  });

  beforeEach(async() => {
    app.jobs = [];
    app.admins = [];
  })

  test('Escrow is created', async() => {
    let payload = testdata.inputs.escrow;
    let res = await escrow.handlePayload(payload, null)
    expect(app.jobs[0]).toMatchObject(testdata.outputs.escrow[0]);
  });

  test('Escrow is released', async() =>{
    await newEscrow()
    let payload = testdata.inputs.release;
    let res = await escrow.handlePayload(payload, null)
    expect(app.jobs[0]).toMatchObject(testdata.outputs.released[0]);
  });

  test('Escrow is closed', async() =>{
    await releasedEscrow()
    let payload = testdata.inputs.disburse;
    let res = await escrow.handlePayload(payload, null)
    expect(app.jobs[0]).toMatchObject(testdata.outputs.disbursed[0]);
    });

  test('Refunded tx is ignored', async() =>{
    let payload = testdata.inputs.refund;
    let res = await escrow.handlePayload(payload, null)
    expect(res).toBe(true) 
  });
});

describe('DUPLICATE SCENARIOS ', () => {

  beforeAll(async() => {
    return await binance.binanceConnect()
  });

  test('Duplicate escrow fails', async() => {
    await newEscrow();
    let payload = testdata.inputs.escrow;
    let res = await escrow.handlePayload(payload, null);
    expect(res.message).toBe('ESCROW_DUPLICATE')
  });

  test('Duplicate release fails', async() => {
    await releasedEscrow()
    let payload = testdata.inputs.release;
    let res = await escrow.handlePayload(payload, null);
    expect(res.message).toBe('RELEASE_DUPLICATE')
  });
});

describe('MALFORMED MEMOS', () => {

  beforeAll(async() => {
    return await binance.binanceConnect()
  });

  test('Empty memo fails', async() => {
    let payload = testdata.inputs.escrowEmptyMemo;
    let res = await escrow.handlePayload(payload, null);
    expect(res.message).toBe('MEMO_EMPTY')
  });

  test('Release bad job fails', async() => {
    await newEscrow();
    let payload = testdata.inputs.badRelease;
    let res = await escrow.handlePayload(payload, null);
    expect(res.message).toBe('RELEASE_NOJOB')
  });

  test('Escrow missing payee fails', async() => {
    let payload = testdata.inputs.escrowNoProvider;
    let res = await escrow.handlePayload(payload, null);
    expect(res.message).toBe('MEMO_BAD')
  });

  test('Escrow missing USD value fails', async() => {
    let payload = testdata.inputs.escrowNoUSD;
    let res = await escrow.handlePayload(payload, null);
    expect(res.message).toBe('MEMO_BAD')
  });

  test('Escrow with self fails', async() => {
    let payload = testdata.inputs.escrowSelf;
    let res = await escrow.handlePayload(payload, null);
    expect(res.message).toBe('MEMO_BAD')
  });

  test('Escrow with escrow service fails', async() => {
    let payload = testdata.inputs.payerSelf;
    let res = await escrow.handlePayload(payload, null);
    expect(res.message).toBe('MEMO_BAD')
  });
});


describe('FUNDING SCENARIOS', () => {

  beforeAll(async() => {
    return await binance.binanceConnect()
  });

  test('Fund command rejected if escrow is live', async() => {
    let payload = testdata.inputs.fund;
    let res = await escrow.handlePayload(payload, null);
    expect(res.message).toBe('FUND_LATE')
  });

  test('Fund command accepted if no escrow', async() => { 
    app.admins = []
    app.jobs = []
    let payload = testdata.inputs.fund;
    let res = await escrow.handlePayload(payload, null);
    expect(res.length).toBe(4)
  });
});

describe('REFUND SCENARIOS', () => {
  
  beforeAll(async() => {
    return await binance.binanceConnect()
  });

  beforeEach(async() => {
    app.jobs = [];
    app.admins = [];
  })

  test('Admin refund request is processed', async() => { 
    app.admins = testdata.inputs.admins;
    let payload = testdata.inputs.escrow;
    let res = await escrow.handlePayload(payload, null);
    payload = testdata.inputs.adminrefund;
    let res2 = await escrow.handlePayload(payload, null);
    expect(res2.status).toBe('REFUNDING')
  });

  test('Admin refund results in refund', async() => { 
    app.admins = testdata.inputs.admins;
    let payload = testdata.inputs.escrow;
    let res = await escrow.handlePayload(payload, null);
    payload = testdata.inputs.adminrefund;
    let res2 = await escrow.handlePayload(payload, null);
    payload = testdata.inputs.jobrefund;
    let res3 = await escrow.handlePayload(payload, null);
    expect(res3.status).toBe('REFUNDED')
  });

  test('Payee refund request is processed', async() => { 
    let payload = testdata.inputs.escrow;
    let res = await escrow.handlePayload(payload, null);
    payload = testdata.inputs.payeerefund;
    let res2 = await escrow.handlePayload(payload, null);
    expect(res2.status).toBe('REFUNDING')
  });

  test('Client refund request is rejected', async() => { 
    let payload = testdata.inputs.escrow;
    let res = await escrow.handlePayload(payload, null);
    payload = testdata.inputs.clientrefund;
    let res2 = await escrow.handlePayload(payload, null);
    expect(res2.message).toBe('REFUND_ERROR')
  });
});

describe('OTHER TESTS', () => {

  beforeAll(async() => {
    return await binance.binanceConnect()
  });

  test('transfer works', async() => {
    await releasedEscrow()
    let order = app.jobs[0];
    let res = await binance.transferToken(order,'RELEASE')
    expect(res).toBeDefined();
  });

  
});